﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RadioActivity.Test
{
    [TestClass]
    public class RadioactivityService_ImportRadioactivityPlots_Tests
    {
        private IPlotDataAccess plotDataAccess;
        private IGeonameService geonameService;

        [TestInitialize]
        public void SetUp()
        {
            geonameService = new FakeGeonameService()
                .Add("Bordeaux", 33)
                .Add("Paris", 75)
                .Add("Créteil", 94)
                .Add("Maison-Alfort", 94)
                .Add("Lyon", 69);
        }

        [TestMethod]
        public void ImportPlots_ShouldJustWork()
        {
            plotDataAccess = new FakePlotDataAccess();
            var service = new RadioactivityService(plotDataAccess, geonameService);

            service.ImportRadioactivityPlots(new[]
            {
                new RadioactivityPlot(new DateTime(1999, 12, 4), "Paris", 28.3m),
                new RadioactivityPlot(new DateTime(2012, 2, 13), "Paris", 62.3m),
                new RadioactivityPlot(new DateTime(2014, 7, 25), "Lyon", 85.3m)
            });

            //NOTE: 
            //      Je fais un assert sur les méthodes déjà créées pour vérifier l'import.
            //      Il y a peut être moyen de faire mieux.
            var radioactivities = service.GetDepartmentsMeanRadioactivities().ToList();

            Assert.IsTrue(radioactivities.Contains(new DepartmentRadioactivity(75, 62.3m)));
            Assert.IsTrue(radioactivities.Contains(new DepartmentRadioactivity(69, 85.3m)));

            var history = service.GetDepartmentRadioactivityHistory(75).ToList();

            Assert.IsTrue(history.Contains(new TimestampedRadioactivity(Year(1999), 28.3m)));
            Assert.IsTrue(history.Contains(new TimestampedRadioactivity(Year(2012), 62.3m)));
        }

        public static DateTime Year(int year)
        {
            return new DateTime(year, 1, 1);
        }
    }
}