﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RadioActivity.Test
{
    [TestClass]
    public class RadioactivityService_GetDepartmentsMeanRadioactivities_Tests
    {
        private IPlotDataAccess plotDataAccess;
        private IGeonameService geonameService;

        [TestInitialize]
        public void SetUp()
        {
            geonameService = new FakeGeonameService()
                .Add("Bordeaux", 33)
                .Add("Paris", 75)
                .Add("Créteil", 94)
                .Add("Maison-Alfort", 94);
        }

        [TestMethod]
        public void NoPlots_ShouldGiveAnEmptySet()
        {
            plotDataAccess = new FakePlotDataAccess(new RadioactivityPlot[0]);
            var service = new RadioactivityService(plotDataAccess, geonameService);
            
            var deparmentRadioactivities = service.GetDepartmentsMeanRadioactivities();

            Assert.IsFalse(deparmentRadioactivities.Any());
        }

        [TestMethod]
        public void APlotAtParis_ShouldGiveAPlotForDepartment75()
        {
            plotDataAccess = new FakePlotDataAccess(new []
            {
                new RadioactivityPlot(DateTime.Today, "Paris", 28.5m), 
            });
            var service = new RadioactivityService(plotDataAccess, geonameService);

            var deparmentRadioactivities = service.GetDepartmentsMeanRadioactivities();

            Assert.IsTrue(deparmentRadioactivities.Contains(new DepartmentRadioactivity(75, 28.5m)));
        }

        [TestMethod]
        public void TwoPlotsAtTwoCitiesOfADepartment_ShouldGiveAPlotForDepartment94()
        {
            plotDataAccess = new FakePlotDataAccess(new[]
            {
                new RadioactivityPlot(DateTime.Today, "Créteil", 15m), 
                new RadioactivityPlot(DateTime.Today, "Maison-Alfort", 20m), 
            });
            var service = new RadioactivityService(plotDataAccess, geonameService);

            var deparmentRadioactivities = service.GetDepartmentsMeanRadioactivities();

            Assert.IsTrue(deparmentRadioactivities.Contains(new DepartmentRadioactivity(94, 17.5m)));
        }

        [TestMethod]
        public void TwoPlotsAtBordeaux_ShouldGiveTheMostRecentPlotForDepartment33()
        {
            plotDataAccess = new FakePlotDataAccess(new[]
            {
                new RadioactivityPlot(new DateTime(2015, 1, 1), "Bordeaux", 30m), 
                new RadioactivityPlot(new DateTime(2015, 2, 2), "Bordeaux", 20m), 
            });
            var service = new RadioactivityService(plotDataAccess, geonameService);

            var deparmentRadioactivities = service.GetDepartmentsMeanRadioactivities();

            Assert.IsTrue(deparmentRadioactivities.Contains(new DepartmentRadioactivity(33, 20m)));
        }

        [TestMethod]
        public void SomePlotsInDifferentLocation_ShouldGiveTheMostRecentPlotByDepartments()
        {
            plotDataAccess = new FakePlotDataAccess(new[]
            {
                new RadioactivityPlot(new DateTime(2015, 1, 1), "Bordeaux", 30m), 
                new RadioactivityPlot(new DateTime(2015, 2, 2), "Bordeaux", 20m), 
                new RadioactivityPlot(new DateTime(2015, 1, 12), "Créteil", 20m), 
                new RadioactivityPlot(new DateTime(2015, 1, 12), "Maison-Alfort", 15m), 
                new RadioactivityPlot(new DateTime(2015, 5, 22), "Maison-Alfort", 5m), 
            });
            var service = new RadioactivityService(plotDataAccess, geonameService);

            var deparmentRadioactivities = service.GetDepartmentsMeanRadioactivities().ToList();

            Assert.IsTrue(deparmentRadioactivities.Contains(new DepartmentRadioactivity(33, 20m)));
            Assert.IsTrue(deparmentRadioactivities.Contains(new DepartmentRadioactivity(94, 12.5m)));
        }
    }
}