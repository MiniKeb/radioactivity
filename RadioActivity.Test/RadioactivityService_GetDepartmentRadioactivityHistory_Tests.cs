﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RadioActivity.Test
{
    [TestClass]
    public class RadioactivityService_GetDepartmentRadioactivityHistory_Tests
    {
        private IPlotDataAccess plotDataAccess;
        private IGeonameService geonameService;

        [TestInitialize]
        public void SetUp()
        {
            geonameService = new FakeGeonameService()
                .Add("Bordeaux", 33)
                .Add("Paris", 75)
                .Add("Créteil", 94)
                .Add("Maison-Alfort", 94);
        }

        [TestMethod]
        public void NoPlots_ShouldGiveAnEmptySet()
        {
            plotDataAccess = new FakePlotDataAccess(new RadioactivityPlot[0]);
            var service = new RadioactivityService(plotDataAccess, geonameService);

            var history = service.GetDepartmentRadioactivityHistory(33);

            Assert.IsFalse(history.Any());
        }

        [TestMethod]
        public void SomePlotsAtParisIn2014_ShouldGiveOneMeanHistoryPlot()
        {
            plotDataAccess = new FakePlotDataAccess(new []
            {
                new RadioactivityPlot(new DateTime(2014, 1, 2), "Paris", 10m),
                new RadioactivityPlot(new DateTime(2014, 5, 28), "Paris", 15m),
                new RadioactivityPlot(new DateTime(2014, 9, 15), "Paris", 28.4m),
                new RadioactivityPlot(new DateTime(2014, 12, 5), "Paris", 30m),
            });
            var service = new RadioactivityService(plotDataAccess, geonameService);

            var history = service.GetDepartmentRadioactivityHistory(75);

            Assert.IsTrue(history.Contains(new TimestampedRadioactivity(Year(2014), 20.85m)));
        }

        [TestMethod]
        public void SomePlotsInDifferentCitiesOfDifferentDepartments_ShouldGiveAMeanHistoryPlotFor75()
        {
            plotDataAccess = new FakePlotDataAccess(new[]
            {
                new RadioactivityPlot(new DateTime(2014, 1, 2), "Créteil", 10m),
                new RadioactivityPlot(new DateTime(2014, 5, 28), "Paris", 15m),
                new RadioactivityPlot(new DateTime(2014, 9, 15), "Maison-Alfort", 28.4m),
                new RadioactivityPlot(new DateTime(2014, 12, 5), "Paris", 30m),
            });
            var service = new RadioactivityService(plotDataAccess, geonameService);

            var history = service.GetDepartmentRadioactivityHistory(75);

            Assert.IsTrue(history.Contains(new TimestampedRadioactivity(Year(2014), 22.5m)));
        }

        [TestMethod]
        public void SomePlotsAtParisBetween2010And2014_ShouldGive5MeanHistoryPlots()
        {
            plotDataAccess = new FakePlotDataAccess(new[]
            {
                new RadioactivityPlot(new DateTime(2010, 5, 2), "Paris", 10m),
                new RadioactivityPlot(new DateTime(2010, 7, 7), "Paris", 20.5m),
                new RadioactivityPlot(new DateTime(2011, 6, 18), "Paris", 40.3m),
                new RadioactivityPlot(new DateTime(2012, 2, 21), "Paris", 25.8m),
                new RadioactivityPlot(new DateTime(2013, 10, 30), "Paris", 10.4m),
                new RadioactivityPlot(new DateTime(2014, 3, 15), "Paris", 15m),
                new RadioactivityPlot(new DateTime(2014, 5, 5), "Paris", 30m),
            });
            var service = new RadioactivityService(plotDataAccess, geonameService);

            var history = service.GetDepartmentRadioactivityHistory(75).ToList();

            Assert.IsTrue(history.Contains(new TimestampedRadioactivity(Year(2010), 15.25m)));
            Assert.IsTrue(history.Contains(new TimestampedRadioactivity(Year(2011), 40.3m)));
            Assert.IsTrue(history.Contains(new TimestampedRadioactivity(Year(2012), 25.8m)));
            Assert.IsTrue(history.Contains(new TimestampedRadioactivity(Year(2013), 10.4m)));
            Assert.IsTrue(history.Contains(new TimestampedRadioactivity(Year(2014), 22.5m)));
        }

        public static DateTime Year(int year)
        {
            return new DateTime(year, 1, 1);
        }
    }
}