﻿using System.Collections.Generic;

namespace RadioActivity.Test
{
    public class FakeGeonameService : IGeonameService
    {
        private readonly Dictionary<string, int> cityDepartments;

        public FakeGeonameService()
        {
            cityDepartments = new Dictionary<string, int>();
        }

        public FakeGeonameService Add(string city, int department)
        {
            cityDepartments.Add(city, department);
            return this;
        }

        public int GetDepartment(string cityName)
        {
            if (cityDepartments.ContainsKey(cityName))
                return cityDepartments[cityName];

            return 0;
        }
    }
}