using System.Collections.Generic;

namespace RadioActivity.Test
{
    public class FakePlotDataAccess : IPlotDataAccess
    {
        private readonly List<RadioactivityPlot> radioactivityPlots;

        public FakePlotDataAccess()
        {
            radioactivityPlots = new List<RadioactivityPlot>();
        }

        public FakePlotDataAccess(IEnumerable<RadioactivityPlot> radioactivityPlots)
        {
            this.radioactivityPlots = new List<RadioactivityPlot>(radioactivityPlots);
        }

        public void AddPlots(IEnumerable<RadioactivityPlot> radioactivityPlots)
        {
            this.radioactivityPlots.AddRange(radioactivityPlots);
        }

        public IEnumerable<RadioactivityPlot> GetAllPlots()
        {
            return radioactivityPlots;
        }
    }
}