﻿using System.Collections.Generic;

namespace RadioActivity
{
    public interface IPlotDataAccess
    {
        IEnumerable<RadioactivityPlot> GetAllPlots();
        void AddPlots(IEnumerable<RadioactivityPlot> radioactivityPlots);
    }
}