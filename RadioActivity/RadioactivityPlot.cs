﻿using System;

namespace RadioActivity
{
    public class RadioactivityPlot
    {
        public DateTime MeasureDate { get; private set; }
        public string CityName { get; private set; }
        public decimal BecquerelAmount { get; private set; }

        public RadioactivityPlot(DateTime measureDate, string cityName, decimal becquerelAmount)
        {
            MeasureDate = measureDate;
            CityName = cityName;
            BecquerelAmount = becquerelAmount;
        }
    }
}