using System;

namespace RadioActivity
{
    public class TimestampedRadioactivity
    {
        public DateTime Date { get; private set; }
        public decimal MeanRadioactivity { get; private set; }

        public TimestampedRadioactivity(DateTime date, decimal meanRadioactivity)
        {
            Date = date;
            MeanRadioactivity = meanRadioactivity;
        }

        protected bool Equals(TimestampedRadioactivity other)
        {
            return Date.Equals(other.Date) && MeanRadioactivity == other.MeanRadioactivity;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TimestampedRadioactivity) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Date.GetHashCode()*397) ^ MeanRadioactivity.GetHashCode();
            }
        }
    }
}