using System;
using System.Collections.Generic;
using System.Linq;

namespace RadioActivity
{
    public class RadioactivityService
    {
        private readonly IPlotDataAccess plotDataAccess;
        
        //NOTE: Je fais appelle � un service ext�rieur pour obtenir les d�partements � partir du nom des villes.
        private readonly IGeonameService geonameService;

        public RadioactivityService(IPlotDataAccess plotDataAccess, IGeonameService geonameService)
        {
            this.plotDataAccess = plotDataAccess;
            this.geonameService = geonameService;
        }

        public IEnumerable<DepartmentRadioactivity> GetDepartmentsMeanRadioactivities()
        {
            var radioactivityPlots = plotDataAccess.GetAllPlots().ToList();
            if (!radioactivityPlots.Any())
                return new DepartmentRadioactivity[0];
            
            //NOTE : 
            //      Ceci pourrait �tre revu car on risque d'appeler trop souvent le service Geoname.
            //      Je consid�re qu'on souhaite la moyenne sur les plus r�centes des donn�es disponibles.
            //      Je consid�re que le seuillage se fera au niveau de l'interface.
            return radioactivityPlots
                .GroupBy(plot => plot.CityName)
                .Select(byCity => byCity.OrderByDescending(plot => plot.MeasureDate).First())
                .GroupBy(plot => geonameService.GetDepartment(plot.CityName))
                .Select(groupByDepartment => new DepartmentRadioactivity(groupByDepartment.Key, GetMean(groupByDepartment)));
        }

        private static decimal GetMean(IEnumerable<RadioactivityPlot> plots)
        {
            var radioactivityPlots = plots.ToList();
            var sum = radioactivityPlots.Sum(plot => plot.BecquerelAmount);
            return sum / radioactivityPlots.Count();
        }

        public IEnumerable<TimestampedRadioactivity> GetDepartmentRadioactivityHistory(int departmentNumber)
        {
            var departmentPlots = plotDataAccess.GetAllPlots()
                .Where(plot => geonameService.GetDepartment(plot.CityName) == departmentNumber)
                .ToList();

            if (!departmentPlots.Any())
                return new TimestampedRadioactivity[0];

            // NOTE:
            //      Je consid�re que nous souhaitons la moyenne de l'ensemble des valeurs relev�es sur l'ann�e pour un d�partement.
            return departmentPlots.GroupBy(plot => plot.MeasureDate.Year)
                .Select(@group => new TimestampedRadioactivity(new DateTime(@group.Key, 1, 1), GetMean(@group)));
        }

        public void ImportRadioactivityPlots(IEnumerable<RadioactivityPlot> radioactivityPlots)
        {
            plotDataAccess.AddPlots(radioactivityPlots);
        }
    }
}