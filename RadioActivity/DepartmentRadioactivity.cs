namespace RadioActivity
{
    public class DepartmentRadioactivity
    {
        public int DepartmentNumber { get; private set; }
        public decimal RadioactityMeasure { get; private set; }

        public DepartmentRadioactivity(int departmentNumber, decimal radioactityMeasure)
        {
            DepartmentNumber = departmentNumber;
            RadioactityMeasure = radioactityMeasure;
        }

        protected bool Equals(DepartmentRadioactivity other)
        {
            return DepartmentNumber == other.DepartmentNumber && RadioactityMeasure == other.RadioactityMeasure;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DepartmentRadioactivity) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (DepartmentNumber*397) ^ RadioactityMeasure.GetHashCode();
            }
        }
    }
}