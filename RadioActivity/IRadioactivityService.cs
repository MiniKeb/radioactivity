using System.Collections.Generic;
using System.Linq;

namespace RadioActivity
{
    public interface IRadioactivityService
    {
        IEnumerable<DepartmentRadioactivity> GetDepartmentsMeanRadioactivities();
        IOrderedEnumerable<TimestampedRadioactivity> GetDepartmentRadioactivityHistory();
    }
}