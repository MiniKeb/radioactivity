namespace RadioActivity
{
    public interface IGeonameService
    {
        int GetDepartment(string cityName);
    }
}